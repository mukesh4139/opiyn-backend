FactoryGirl.define do
  factory :item do |f|
    f.id "7d6cfd8c-8924-c676-c0b44fc5707e"
    f.title "Over a cup of coffee"
    f.image_url "https://opiyn.s3.amazonaws.com/2016/05/17/07/25/13/880/file."
    f.reviews_count 1
  end


end