require 'spec_helper'

RSpec.describe DashboardController, type: :controller do
  describe "GET #index" do
    it "should render :index view" do
      get :index
      response.should render_template :index
    end
  end

end
