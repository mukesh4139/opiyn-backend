# Factory girl dsl added so that methods can be called directly
# Ex: create(:event)
# Ex: build(:book)
RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods
end