== README

# Opiyn-Backend

This is a rails backend system with two models - Item and Review. Item is any object which can be reviewed. Each item in the backend can have many reviews.

## Dependencies
            * Ruby Version    : 2.1.2
            * Rails           : 4.2.6
            * faker
            * active_model_serializer
            * kaminari
            * rspec
            * factory_girl_rails

## Installation
            $ git clone https://mukesh4139@bitbucket.org/mukesh4139/opiyn-backend.git
            $ cd opiyn-backend
            $ bundle install
            $ rake db:migrate
            $ rake db:seed (optional)
            $ rails s


## Documentation

For Serializers, active_model_serializers has been used.

            $ rails g serializer item
            $ rails g serializer review

For Pagination, kaminari gem is used for pagination.

For Testing, rspec along with factory_girl_rails has been used.

            $ rspec spec/controllers/
            $ rspec spec/models/



