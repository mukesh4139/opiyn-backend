class AddItemIdToReview < ActiveRecord::Migration
  def change
    add_column :reviews, :item_id, :string
  end
end
