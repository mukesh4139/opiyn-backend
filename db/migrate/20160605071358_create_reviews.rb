class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews, {id: false} do |t|
      t.string :id
      t.string :content

      t.timestamps null: false
    end
  end
end
