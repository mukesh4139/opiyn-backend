# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'securerandom'
10.times do |n|
  id = SecureRandom.hex(4) + '-' + SecureRandom.hex(2) + '-' + SecureRandom.hex(2) + '-' + SecureRandom.hex(6)
  title = Faker::Lorem.sentence
  image_url = Faker::Avatar.image
  Item.create!( id: id,
                title: title,
                image_url: image_url)
end

item_ids = Item.all.ids
size = item_ids.length

100.times do |n|
  index = Random.rand(size-1)
  id = SecureRandom.hex(4) + '-' + SecureRandom.hex(2) + '-' + SecureRandom.hex(2) + '-' + SecureRandom.hex(6)
  content = Faker::Lorem.sentence
  item_id = item_ids[index]
  Review.create!( id: id,
                  content: content,
                  item_id: item_id)
end

