class ItemSerializer < ActiveModel::Serializer
  attributes :id, :title, :image_url, :counters, :created_at
  has_many :reviews

  # reversing the order of reviews returns latest first.
  def reviews
    Review.limited_reviews.reverse
  end
end
