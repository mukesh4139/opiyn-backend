# == Schema Information
#
# Table name: reviews
#
#  id                :string          not null, primary key
#  content           :string
#  item_id           :string

class Review < ActiveRecord::Base
  # making own id column act as primary key
  self.primary_key = :id

  # adding belongs to association to item model, adding cache to update count of reviews everytime review of an item is added
  belongs_to :item, counter_cache: :reviews_count

  #limiting only 10 reviews
  scope :limited_reviews, lambda {limit(10)}
end
