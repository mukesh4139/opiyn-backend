# == Schema Information
#
# Table name: items
#
#  id                :string          not null, primary key
#  title             :string
#  image_url         :string

class Item < ActiveRecord::Base
  # making own id column act as primary key
  self.primary_key = :id

  #addding has_many associtation to review model
  has_many :reviews

  def counters
    {reviews: self.reviews_count}
  end
end
