class ItemsController < ApplicationController
  before_action :set_item, only: [:show]

  # GET /items
  # returning data only in json format
  def index
    @export_items = Item.order("reviews_count DESC")
    @items = @export_items.page(params[:page]).per(10)
    render json: @items
  end

  # GET /items/1
  # returning data only in json format
  def show
    render json: @item
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_item
    params_length = params[:id].length
    id = params[:id].slice(2, params_length-4)
    @item = Item.find(id)
  end

end
